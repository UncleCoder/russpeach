(function(rsp, $, undefined) {
    rsp.ShowMap = function() {
        var i;
        var wholeBounds = new google.maps.LatLngBounds();

        var map = new google.maps.Map(document.getElementById('map'),
                {center: new google.maps.LatLng(0,0)});
        var mc = new MarkerClusterer(map,[],{gridSize:30});
        for (i = 0; i < rsp.data.length; i++) {
            var point = new google.maps.LatLng(rsp.data[i][0], rsp.data[i][1]);
            wholeBounds.extend(point);
            var marker = new google.maps.Marker({
                position: point,
                map: map,
                icon: '/img/marker.png'
            });
            marker.set('route', rsp.data[i][2]);
            google.maps.event.addListener(marker, 'click', function() {
                var href= document.location.href;
                var path = href+this.get('route');
                document.location.href=path;
            });
            mc.addMarker(marker);
        }

        map.fitBounds(wholeBounds);
    };
    google.maps.event.addDomListener(window, 'load', rsp.ShowMap);
}(window.rsp = window.rsp || {}, jQuery));
