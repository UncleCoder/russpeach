(function(rsp, $, undefined) {

    rsp.initialize = function() {
        var coord=new google.maps.LatLng(rsp.coord[0],rsp.coord[1])
        var mapOptions = {
            center: coord,
            zoom: 12
        };
        var map = new google.maps.Map(document.getElementById('map'),
                mapOptions);
        new google.maps.Marker({
            position: coord,
            map: map,
            icon: '/img/marker.png',
        });
    };

    google.maps.event.addDomListener(window, 'load', rsp.initialize);
}(window.rsp = window.rsp || {}, jQuery));
