(function(rsp, $, undefined) {

    rsp.initialize = function() {
        var lat = $.cookie('lat') * 1;
        var lng = $.cookie('lng') * 1;
        var zoom = $.cookie('zoom') * 1;

        var initialLocation = new google.maps.LatLng(55.755826, 37.6173);
        if (isNaN(lat && lng && zoom)) {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                    map.setCenter(initialLocation);
                }, function() {
                    map.setCenter(initialLocation);
                });
            }
        } else {
            initialLocation = new google.maps.LatLng(lat, lng);
        }

        var mapOptions = {
            center: initialLocation,
            zoom: zoom||13
        };
        var map = new google.maps.Map(document.getElementById('map'),
                mapOptions);
        rsp.map=map;
        google.maps.event.addListener(map, 'idle', function(){rsp.newPosition()});
        var input = $('[name="PROPERTY[105][0]"]')[0];
        var autocomplete = new google.maps.places.Autocomplete(input, {types: ['geocode']});
        autocomplete.bindTo('bounds', map);

        var geocoder = new google.maps.Geocoder();

        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                return;
            }

            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(16);
            }
            if (!rsp.marker) {
                rsp.marker = new google.maps.Marker({
                    position: place.geometry.location,
                    map: map,
                    icon: '/img/marker.png',
                    draggable: true
                });
            } else {
                rsp.marker.setPosition(place.geometry.location);
            }
            rsp.newPosition(place.geometry.location);
            google.maps.event.addListener(rsp.marker, 'dragend', function() {
                var position = this.getPosition();
                geocoder.geocode({'latLng': position}, function(results, status) {
                    var componentForm = {
                        street_number: {name_type: 'short_name', pos: 7, val: ""},
                        route: {name_type: 'long_name', pos: 6, val: ""},
                        locality: {name_type: 'long_name', pos: 5, val: ""},
                        administrative_area_level_1: {name_type: 'short_name', pos: 3, val: ""},
                        administrative_area_level_2: {name_type: 'short_name', pos: 4, val: ""},
                        country: {name_type: 'long_name', pos: 1, val: ""},
                        postal_code: {name_type: 'short_name', pos: 2, val: ""}
                    };

                    if (status === google.maps.GeocoderStatus.OK) {
                        for (var i = 0; i < results[0].address_components.length; i++) {
                            var addressType = results[0].address_components[i].types[0];
                            if (componentForm[addressType]) {
                                var val = results[0].address_components[i][componentForm[addressType].name_type];
                                componentForm[addressType].val = val;
                            }
                        }
                        if (componentForm.administrative_area_level_2.val.indexOf("г. ") === 0) {
                            componentForm.administrative_area_level_2.val = '';
                        }
                        var ret = [];
                        for (var comp in componentForm) {
                            if (componentForm[comp].val !== '') {
                                ret[componentForm[comp].pos] = componentForm[comp].val;
                            }
                        }
                        ret = rsp.cleanArray(ret);
                        $('[name="PROPERTY[105][0]"]').val(ret.join(", "));
                        rsp.newPosition(position);
                    }
                });
            });
        });

        rsp.cleanArray = function(actual) {
            var newArray = new Array();
            for (var i = 0; i < actual.length; i++) {
                if (actual[i]) {
                    newArray.push(actual[i]);
                }
            }
            return newArray;
        };

        rsp.newPosition = function(pos) {
            pos=pos||rsp.map.getCenter();
            $('#coord').val(JSON.stringify([pos.lat(), pos.lng()]));
            $.cookie('lat', pos.lat());
            $.cookie('lng', pos.lng());
            $.cookie('zoom', rsp.map.getZoom());
        };

    };

    google.maps.event.addDomListener(window, 'load', rsp.initialize);
}(window.rsp = window.rsp || {}, jQuery));
